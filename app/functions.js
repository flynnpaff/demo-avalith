module.exports = function (settings, models) {

	var render = {};

	/*-----------------------------------------------------
	----------------------ACCOUNT/USER---------------------
	-----------------------------------------------------*/
	
	//login
	render.account_login = function (req, res){
		console.log("[i] render: login");
		var login_email = req.body.email;
		var login_password = req.body.password;

		//TO DO: JWT, login.

		//verificamos si existe email y password
		if(!login_email || !login_password){
			res.json({message: 'Error al intentar loguear: user/password requeridos'});
		}else{
			var params = {
				email : login_email,
				password: login_password
			}
			//si existe validamos el el mismo
			models.accountLogin(params, function(error, user){

				if(error){
					res.json({message : 'Error al intentar iniciar sesión'});
					return;
				}

				//si los datos son erroneos...
				if(!user){
					console.log("[!] login: datos ingresados incorrectos");
					res.json({message : 'Los datos ingresados son inválidos'});
					return;
				}
				
				console.log("[i] login: inicio de sesión exitoso");

				res.status(201).json(user);
			});
		}
	};

	//logout
	render.account_logout = function (req, res){
		console.log("[i] render: logout");
		res.json({});
	};

	//change password
	render.account_change_password = function (req, res){
		console.log("[i] render: change-password");
		var nuevo_password = req.body.password;

		if(!nuevo_password){
			res.json({message: 'Error al cambiar la clave, el campos password es requerido'});
		}else{
			var params = {
				password: nuevo_password
			}

			models.changePassword(params, function(error, user){
				if(error){
					res.json({message : 'Error al intentar iniciar sesión'});
					return;
				}

				console.log("[i] se cambió el password correctamente")

				res.json(user);
			});
		}
	};

	//reset password
	render.account_reset_password = function (req, res){
		console.log("[i] render: reset-password");
		res.json({});
	};


	//registrar teatro
	render.register_theatre = function (req, res){
		console.log("[i] render: register theatre");
		
		var params = req.body;
		var params_user = req.body.user || null;
		var params_theatre = req.body.theatre || null;

		if(!params_user || !params_user.fullname || !params_user.email || !params_user.phone || !params_user.password || !params_theatre || !params_theatre.name|| !params_theatre.address || !params_theatre.phone || !params_theatre.email || !params_theatre.history || !params_theatre.country || !params_theatre.province || !params_theatre.city || !params_theatre.circuit_type){
			console.log("Error al registrar el teatro, faltan campos requeridos");
			res.json({message: '[!] Error al registrar el teatro, faltan campos requeridos'})
			return;
		}else{
			//registramos el teatro
			models.registerTheatre(params, function(err, new_theatre){
				if (err) throw err;

				res.json(new_theatre);
			});

		}
	};

	//register_event
	render.register_event = function (req, res){
		console.log("[i] render: register_event");
		res.json({});
	};

	//forgot_password
	render.forgot_password = function (req, res){
		console.log("[i] render: forgot_password");
		res.json({});
	};

	//update_account
	render.update_account = function (req, res){
		console.log("[i] render: update_account");
		res.json({});
	};

	/*-----------------------------------------------------
	-------------------------THEATRE-----------------------
	-----------------------------------------------------*/

	render.theatres = function (req, res){
		console.log("[i] render: theatres");
		
		models.getTheatresAll(function(err, theatres){
			if (err) throw err;

			res.json(theatres);

		});
	};

	render.theatres_getone = function (req, res){
		var theatre_id = req.params.id;
		console.log("[i] render: theatres profiles");
		res.json({});
	};

	render.theatres_profiles = function (req, res){
		console.log("[i] render: theatres profiles");
		res.json({});
	};

	render.theatres_update = function (req, res){
		console.log("[i] render: theatres update");
		res.json({});
	};

	return render;
};