module.exports = function (settings){

	//incluimos paquetes a utilizar
	var mongoose = require('mongoose');
	var Schema = mongoose.Schema;

	//conectamos a la DB
	mongoose.connect(settings.db, {useNewUrlParser: true});

	//eventos de la conexión a la DB
	var db = mongoose.connection;
	db.on('connected', function () {  
	  console.log('[i] Conexión existosa a la DB: ' + settings.db);
	}); 
	db.on('error',function (err) {  
	  console.log('[!] Error al conectar la DB: ' + err);
	}); 
	db.on('disconnected', function (err) {  
	  console.log('[i] Conexión con la DB desconectada'); 
	});

	/*-----------------------------------------------------
	-------------------------SCHEMAS-----------------------
	-----------------------------------------------------*/

	//users
	var SchemaUsers = new Schema({
		id : Number
		,type : Number
		,responsability : Number
		,fullName : String
		,phone : String
		,email : String
		,password : String
	});

	//theatres
	var SchemaTheatres = new Schema({
	    id : Number
	    ,name : String
	    ,address : String
	    ,phone : String
	    ,email : String
	    ,site_url : String
	    ,history : String
	    ,country : Number
	    ,province : Number
	    ,profile_image : String
	    ,cover_image : String
	    ,city : Number
	    ,circuit_type : Number
	    ,gallery : [{
	    	id :  Number
	        ,src :  String
	        ,alt :  String
	    }]
	});

	/*-----------------------------------------------------
	-------------------------MODELOS-----------------------
	-----------------------------------------------------*/

	var ModeloUsers = mongoose.model('users',SchemaUsers,'users');
	var ModeloTheatres = mongoose.model('theatres',SchemaTheatres,'theatres');

	var models = {};

	/*-----------------------------------------------------
	----------------------MODELOS > USER-------------------
	-----------------------------------------------------*/

	//login
	models.accountLogin = function(params , callback){
		ModeloUsers.findOne(params, function(error, user){
			if(error){
				console.log("Error en login")
			}

			callback(error, user);
		});		 
	}

	//cambiar password
	models.changePassword = function(params , callback){

		ModeloUsers.findOneAndUpdate({}, params, function(error, user){
			if(error){
				console.log("Error en login")
			}

			callback(error, {"password" : user.password});
		});		 
	}


	models.registerTheatre = function(params , callback){

		var newTheatre = new ModeloTheatres(params);

		newTheatre.save(function(error, theatre){
			if( error){
				console.log("[!] Error al insetar el teatro.")
			}
		
			callback(error, theatre);
		});		 

	}


	/*-----------------------------------------------------
	-------------------MODELOS > THEATRES------------------
	-----------------------------------------------------*/

	models.getTheatresAll = function(callback){
		ModeloTheatres.find({}, function(error, theatres){
			if(error){
				console.log("[!] Error al obtener: theatres", error);
			}

			callback(error, theatres)
		});
	}


	return models;

}

