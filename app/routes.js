module.exports = function (settings, model, render){

	//incluimos paquetes a utilizar
	var express = require('express');
	var router = express.Router();

	/*-----------------------------------------------------
	--------------------------HOME-------------------------
	-----------------------------------------------------*/

	router.get('/', function(req, res){
		res.json({message: "API REST Home - Demo Avalith"});
	});

	/*-----------------------------------------------------
	----------------------ACCOUNT/USER---------------------
	-----------------------------------------------------*/

	router.post('/auth/login', render.account_login);
	router.post('/auth/logout', render.account_logout);

	router.post('/user/change-password', render.account_change_password);
	router.post('/user/reset-password', render.account_reset_password);
	router.post('/user/theatre', render.register_theatre);
	router.post('/user/event', render.register_event);
	router.post('/user/forgot-password', render.forgot_password);
	router.put('/user/update-account', render.update_account);

	/*-----------------------------------------------------
	-------------------------THEATRE-----------------------
	-----------------------------------------------------*/

	router.get('/theatres/all', render.theatres);
	router.put('/theatres/update', render.theatres_update);
	router.get('/theatres/profile', render.theatres_profiles);
	router.get('/theatres/:id', render.theatres_getone);

	/*-----------------------------------------------------
	-----------------------THEATRE ROOM--------------------
	-----------------------------------------------------*/

	/*-----------------------------------------------------
	---------------------------EVENT-----------------------
	-----------------------------------------------------*/

	/*-----------------------------------------------------
	---------------------------TOUR------------------------
	-----------------------------------------------------*/

	/*-----------------------------------------------------
	-------------------------PROPOSAL----------------------
	-----------------------------------------------------*/

	/*-----------------------------------------------------
	-------------------------PAYMENTS----------------------
	-----------------------------------------------------*/

	/*-----------------------------------------------------
	-----------------------NOTIFICATIONS-------------------
	-----------------------------------------------------*/

	/*-----------------------------------------------------
	--------------------------AGENDA-----------------------
	-----------------------------------------------------*/

	return router;

}