/*-----------------------------------------------------
-----------------Demo API REST Avalith-----------------
-----------------------------------------------------*/

// incluimos paquetes a utilizar
var express    = require('express');
var mongoose   = require('mongoose');
var bodyParser = require('body-parser');
//var jwt = require('jsonwebtoken'); //to do

var settings   = require('./config/main');
var models     = require('./app/models')(settings);
var render     = require('./app/functions')(settings, models);

//creamos una app express
var app = express();

//parseamos request content-type
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json());

//incluimos las rutas de la app
app.use("/" , require('./app/routes')(settings, models, render));

//middleware error 404
app.use(function(req, res, next){
	res.status(400).send("[!] Ruta inexistente.");
});

//corremos el servidor en el puerto seteado
app.listen(settings.port, function(){
	console.log("App iniciada en el puerto 8000.");
});